import("vuetify/dist/vuetify.min.css")
import("style.styl")

import Vue from "vue"
import VueRouter from "vue-router"
import Vuetify from "vuetify"
import App from "./App.vue"

Vue.use(VueRouter)
Vue.use(Vuetify)

new Vue(App)