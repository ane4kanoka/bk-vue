import baseService from "./baseService"
import { organizationFileHTTP, organizationProjectHTTP } from "./urlProvider"
import { errorAlert } from "../../models/alert"

export default class organizationFileService extends baseService {

    static async getPageMap (documentId) {
        this.commit("ajaxLoading", true)

        try {
            let url = organizationFileHTTP.url(documentId).pageMap
            console.log ('Page map URL: ' + url)
            const response = await this.get(url)

            switch (response.status) {
                case 200:

                    let pageMapResponse = await response.json()
                    console.info("[ Network ]:", "status: ok", "response:", pageMapResponse)

                    return pageMapResponse

                    break

                default:
                    console.error(response.status)
            }
        } catch (e) {

            this.dispatch(
                "notification/showAlert",
                new errorAlert(e.message)
            )

        } finally {
            this.commit("ajaxLoading", false)
        }
    }

    static async getPageMapFullSize (documentId, size) {
        this.commit("ajaxLoading", true)

        try {
            let url = organizationFileHTTP.url(documentId).pageMapFullSize
            console.log ('Page map full size URL: ' + url)
            const response = await this.get(url)

            switch (response.status) {
                case 200:

                    let pageMapResponse = await response.json()
                    console.info("[ Network ]:", "status: ok", "response:", pageMapResponse)

                    return pageMapResponse

                    break

                default:
                    console.error(response.status)
            }
        } catch (e) {

            this.dispatch(
                "notification/showAlert",
                new errorAlert(e.message)
            )

        } finally {
            this.commit("ajaxLoading", false)
        }
    }

    static async getFileList (settings) {
        this.commit("ajaxLoading", true)

        try {
            const response = await this.post(organizationProjectHTTP.url.search, settings)

            switch (response.status) {
                case 200:

                    let fileListResponse = await response.json()
                    console.info("[ Network ]:", "status: ok", "response:", fileListResponse)

                    if (typeof fileListResponse === "object") {

                        if (fileListResponse.hasOwnProperty("Files")) {

                            let fileList = fileListResponse.Files

                            if (Array.isArray(fileList)) {
                                // process user friendly date
                                for (let file of fileList) {
                                    if (file.hasOwnProperty("ChangedOn")) {

                                        const dateTimeUnix = file.ChangedOn
                                        // console.info(`Unix datetime for file ${file.Name}: ${dateTimeUnix}`)
                                        file.date = new Date(dateTimeUnix).toLocaleDateString()
                                    }
                                }

                                return fileList
                            }

                        }
                    }
                    break

                default:
                    console.error(response.status)
            }
        } catch (e) {

            this.dispatch(
                "notification/showAlert",
                new errorAlert(e.message)
            )

        } finally {
            this.commit("ajaxLoading", false)
        }
    }

}