import baseService from "./baseService"
import { organizationProjectHTTP } from "./urlProvider"
import { errorAlert, warningAlert } from "../../models/alert"

import Exception from "./exceptions"

export default class organizationProjectsService extends baseService {

    static async getProjectList() {
        this.commit("ajaxLoading", true)

        try {
            const response = await this.get(organizationProjectHTTP.url.projects)

            switch (response.status) {
                case 200:

                    let projectsListResponse = await response.json()
                    console.info("[ Network ]:", "status: ok", "response:", projectsListResponse)

                    if (Array.isArray(projectsListResponse)) {
                        return projectsListResponse
                    }

                    break

                default:
                    console.error(response.status)
            }
        } catch (e) {

            this.dispatch(
                "notification/showAlert",
                new errorAlert(e.message)
            )

        } finally {
            this.commit("ajaxLoading", false)
        }
    }

}