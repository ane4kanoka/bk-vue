import settings from "settings"

class baseAPI {
    static get url () { return settings.API_ENDPOINT || "api" }
}

class userBasedAPI extends baseAPI {
    static get guestUrl () { return `${super.url}/guest` }
    static get userUrl () { return `${super.url}/user` }
}

class userProfileHTTP extends userBasedAPI {

    static get url () {
        return {
            login: `${this.guestUrl}/login`,
            passwordReset: `${this.guestUrl}/resetpassword`,
            passwordChange: `${this.userUrl}/changepassword`
        }
    }
}

class organizationProjectHTTP extends baseAPI {
    static get url () {
        return {
            projects: `${super.url}/project`,
            search: `${super.url}/search`
        }
    }
}

class organizationFileHTTP extends baseAPI {
    static url (documentId) {
        const base_url = 'https://app.cwbookreport.com/api/image/'

        return {
            pageMap: base_url + documentId + '/preview/page_map.json',
            pageMapFullSize: base_url + documentId + '/fullsize/page_map.json',
            previewImage: base_url + documentId + '/preview/',
            fullSizeImage: base_url + documentId + '/fullsize/',
            thumbnailImage: base_url + documentId + '/thumbnail/'
        }
    }
}

export { userProfileHTTP, organizationProjectHTTP, organizationFileHTTP }