import * as components from './components'
import * as directives from './directives'

function Vuetify (Vue) {
  const Vuetify = components.Vuetify

  Vue.use(Vuetify, {
    components,
    directives
  })
}

export default Vuetify