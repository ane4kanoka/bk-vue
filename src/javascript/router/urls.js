export default {
    fileViewer: "/file/viewer/:documentId/:pageNumber",
    index: "/",
    login: "/login",
    logout: "/logout",
    signup: "/signup",
    profile: "/profile",
    profileChangePassword: "/profile/password",
    profileResetPassword: "/profile/password/reset",
    profileResetPasswordCompleted: "/profile/password/restored",
    project:"/project/:projectID",
    projects:"/projects",
    library: "/library",
    libraryWithSearch: "/library/:search"
}