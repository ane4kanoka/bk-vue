import store from "../store"
import VueRouter from "vue-router"

import urls from "./urls"
// Index
import dashboardComponent from "../components/dashboard.vue"
// Login/Register
import loginComponent from "../components/profile/login.vue"
import signUpComponent from "../components/profile/signup.vue"
// Profile edit
import profileComponent from "../components/profile/profile.vue"
//Project details
import projectDetailsComponent from "../components/projects/projectDetails.vue"
//Project list
import projectListComponent from "../components/projects/projectList.vue"
// Search
import libraryComponent from "../components/library/library.vue"

import fileViewerComponent from "../components/files/fileViewer.vue"

const router = new VueRouter({
    routes: [
        { path: urls.fileViewer, component: fileViewerComponent, name: "File Viewer", props: true },
        { path: urls.index, component: dashboardComponent, name: "Dashboard" },
        { path: urls.login, component: loginComponent, name: "Login" },
        { path: urls.signup, component: signUpComponent, name: "Sign Up" },
        { path: urls.profile, component: profileComponent, name: "Profile" },
        { path: urls.profileChangePassword, component: profileComponent, name: "Profile | New Password" },
        { path: urls.profileResetPassword, component: profileComponent, name: "Profile | Reset Password" },
        { path: urls.profileResetPasswordCompleted, component: profileComponent, name: "Profile | Reset Complete" },
        { path: urls.library, component: libraryComponent, name: "Library" },
        { path: urls.libraryWithSearch, component: libraryComponent, name: "Search in Library" },
        { path: urls.project, component: projectDetailsComponent, name: "Project", props: true },
        { path: urls.projects, component: projectListComponent, name: "Projects" }
    ]
})

// Global hook: change Navbar Header
router.beforeEach((to, from, next) => {
    console.log(`-> Route processing in Global hook to=${to.path}`, to)

    store.commit("setToolbarTitle", to.name)

    switch (to.path) {
        // NO redirect component
        case urls.profileResetPassword:
        case urls.profileResetPasswordCompleted:
            console.log("-> Route NO redirect")
            next()

            break
        // Redirect to index component if User is logged
        case urls.login:
        case urls.signup:

            if (store.state.userProfile.user.active) {
                console.log("-> Route redirect to index")
                next(urls.index)
            }
            else next()

            break
        // Redirect to login component if User is NOT logged
        default:
            if (store.state.userProfile.user.active) {
                console.log("-> Route success for User state")
                next()
            }
            else {
                console.log("-> Route redirect to login")
                next(urls.login)
            }
    }
})

export default router