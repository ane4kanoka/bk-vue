import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex);

import notificationStore from "./notification/notifications"
import userStore from "./account/user"
import organizationStore from "./account/organization"

import router from "../router"
import urls from "../router/urls"

const store = new Vuex.Store({
    modules: {

        notification: notificationStore,
        userProfile: userStore,
        organization: organizationStore

    },
    state: {
        displayProgressBar: false,
        toolbarTitle: "BookReport",
        navigationMenuList: [
            {
                title: "Dashboard",
                icon: "home",
                url: urls.index
            },
            {
                title: "Library",
                icon: "library_books",
                url: urls.library
            },
            {
                title:"Projects",
                icon: "folder_open",
                url: urls.projects
            }
        ]
    },
    mutations: {
        ajaxLoading(state, flag) {
            console.log(`[ Vuex ]: ajax progress: ${flag}`)

            state.displayProgressBar = flag
        },
        setToolbarTitle(state, title) {
            console.log(`[ Vuex ]: changing toolbar title: ${title}`)

            state.toolbarTitle = title || "BookReport"
        }
    },
    actions: {
        navigateTo({ commit, dispatch }, url) {
            console.log(`[ Vuex ]: will navigate to url=${url}`)

            router.push(url)
        },
    },
});

export default store