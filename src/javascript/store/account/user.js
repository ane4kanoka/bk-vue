import User from "../../models/user"
import localStorageService from "../../service/local/storage"
import userProfileService from "../../service/network/userProfileService"
import { warningAlert } from "../../models/alert"

import urls from "../../router/urls"

export default {
    namespaced: true,
    state: {
        user: localStorageService.getUser(),
        profileMenu: [
            {
                title: "Profile",
                icon: "account_circle",
                url: urls.profile
            },
            {
                title: "Change password",
                icon: "mode_edit",
                url: urls.profileChangePassword
            },
            {
                title: "Logout",
                icon: "eject",
                url: urls.logout
            }
        ]
    },
    mutations: {
        loginUser (state, userData) {
            console.log(`[ Vuex ]: OK Auth, saving to localStorage: `, userData)

            state.user = localStorageService.setUser(new User(userData))
        },

        clearUser(state, shouldRemove = false) {
            console.log(`[ Vuex ]: clearUser, shouldRemove = ${shouldRemove}`)

            state.user = localStorageService.setGuest(state.user, shouldRemove)

        }
    },
    actions: {
        saveProfile({ state, commit }) {
            const { firstName, lastName } = state.user;
            console.log(`[ Vuex ]: saving User profile: user.firstName = ${firstName}, user.lastName = ${lastName}`)

            state.user.displayName = `${firstName} ${lastName}`

            localStorageService.setUser(state.user)
        },

        async savePassword({ state, dispatch }, newPassword) {
            console.log(`[ Vuex ]: <savePassword> action in store`)

            try {

                const result = await userProfileService.changePasswordForUser(state.user, newPassword)

                if (result) {
                    console.log("Close component view")
                    dispatch("navigateTo", urls.index, { root: true })
                }

            } catch (networkException) {
                console.error("[ Vuex ]: networkException in changePasswordForUser: ", networkException.message)
            }
        },

        async resetPassword({ dispatch }, emailData) {
            console.log(`[ Vuex ]: <resetPassword> action in store with [email: ${emailData.Email}]`)

            try {
                const result = await userProfileService.resetPasswordToEmail(emailData)

                console.log(`Result in resetPassword:`, result)

                if (result) {
                    console.log("Display sent result component view")
                    dispatch("navigateTo", urls.profileResetPasswordCompleted, { root: true })
                }

            } catch (networkException) {
                console.error("[ Vuex ]: networkException in resetPassword: ", networkException.message)
            }
        },

        async logIn ({ dispatch }, userFormAuthData) {
            console.log(
            `[ Vuex ]: <Login> action in store with [username: ${userFormAuthData.username}]`
            );

            try {
                await userProfileService.logInWithCredentials(userFormAuthData)

                dispatch("navigateTo", urls.projects, { root: true })

            } catch (networkException) {
                console.error("[ Vuex ]: networkException in logIn: ", networkException.message)
            }
        },
        async signUp ({ state, commit, dispatch }, newUserData) {
            console.log(`[ Vuex ]: <Sign Up> action with data: `, newUserData);

            dispatch(
                "notification/showAlert",
                new warningAlert("Registering coming soon..."),
                { root: true }
            );

        },
        logOut ({ state, commit, dispatch }) {
            console.log(`[ Vuex ]: <logOut> action`)

            commit("clearUser")
            dispatch("navigateTo", "/login", { root: true })
        }
    }
}