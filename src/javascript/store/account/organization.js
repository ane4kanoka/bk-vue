import organizationProjectsService from "../../service/network/organizationProjectsService.js"
import organizationFileService from "../../service/network/organizationFileService.js"

class searchParams {
    constructor (query, pageSize = 10, page = 1, projectOnly = false) {

        if (query) this.basic = query

        this.PageSize = pageSize
        this.CurrentPage = page
        this.isProjectFilesOnly = projectOnly
    }
}

export default {
    namespaced: true,
    state: {
        projects: [],
        files: []
    },
    mutations: {
        setProjects(state, projects) {
            console.log("[ Vuex ]: saving projects in state: ", projects)

            state.projects = projects
        },
        setFileList(state, files) {
            console.log("[ Vuex ]: saving files in state: ", files)

            state.files = files
        }
    },
    actions: {
        async loadProjects({ commit }) {
            console.log("[ Vuex ]: loadProjectList() in organization module")

            try {
                const projects = await organizationProjectsService.getProjectList()

                if (projects.length > 0) commit("setProjects", projects)
                else console.log("[ Vuex ]: empty project list")

            } catch (e) {
                console.error("[ Vuex ]: Error loading projects from service")
            }
        },

        async loadFileList({ commit }, searchQuery) {
            console.log("[ Vuex ]: loadFileList() in organization module")

            try {
                const files = await organizationFileService.getFileList(
                    new searchParams(searchQuery)
                )

                if (Array.isArray(files)) commit("setFileList", files)

            } catch (e) {
                console.error("[ Vuex ]: Error loading files from service")
            }
        },

        async loadFileListForBasicSearch({ dispatch }, query) {
            console.log(`[ Vuex ]: loadFileListForBasicSearch(${query}) in organization module`)

            dispatch("loadFileList", query)
        }
    }
}